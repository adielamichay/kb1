<?php

use yii\db\Migration;

/**
 * Class m180429_162744_change_colums
 */
class m180429_162744_change_colums extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
     
            $this->alterColumn( 'article', 'created_at', $this->integer(11));
            $this->alterColumn( 'article', 'updated_at', $this->integer(11));
       
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180429_162744_change_colums cannot be reverted.\n";
        $this->alterColumn( 'article', 'created_at', $this->timestamp());
        $this->alterColumn( 'article', 'updated_at', $this->timestamp());
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180429_162744_change_colums cannot be reverted.\n";

        return false;
    }
    */
}
